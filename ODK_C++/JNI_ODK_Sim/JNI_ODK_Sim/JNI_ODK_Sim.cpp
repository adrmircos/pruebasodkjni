#include "stdafx.h"
#include "ODK_Functions.h"
#include "ODK_Types.h"

#include "tchar.h"
#include <jni.h>
#include <string.h>
#include <iostream>
using namespace std;
#include <cassert>

#include "ADR_Defines.h"

#include "ODK_CpuReadData.h"
#include "ODK_CpuReadWriteData.h"

/*
 * OnLoad() is invoked after the application binary was loaded.
 * @return ODK_SUCCESS       notify, that no error occurs
 *                            - OnRun() will be invoked automatically
 *         any other value   notify, that an error occurs (user defined)
 *                            - OnUnload() will be invoked automatically
 *                            - ODK application will be unloaded automatically
 */
EXPORT_API ODK_RESULT OnLoad (void)
{
	ODK_TRACE("-->ODK Load");
	//JNIinit();
	ODK_TRACE("<--ODK Load");
    return ODK_SUCCESS;
}

/*
 * OnUnload() is invoked before the application binary is unloaded or when
 *            ODK-Host terminates.
 * @return ODK_SUCCESS       notify, that no error occurs
 *         any other value   notify, that an error occurs (user defined)
 *                            - ODK application will be unloaded anyway
 */
EXPORT_API ODK_RESULT OnUnload (void)
{
	ODK_TRACE("ODK UnLoad");
	//jvm->DestroyJavaVM();		//Parece que da problemas
    return ODK_SUCCESS;
}

/*
 * OnRun() is invoked when the CPU transitions to the RUN state and
 *         after OnLoad().
 * @return Does not affect the load operation or state transition.
 */
EXPORT_API ODK_RESULT OnRun (void)
{
	ODK_TRACE("ODK OnRun");
    return ODK_SUCCESS;
}

/*
 * OnStop() is invoked when the CPU transitions to the STOP/SHUTDOWN state
 *          or before unloading the ODK application.
 * @return Does not affect the load operation or state transition.
 */
EXPORT_API ODK_RESULT OnStop (void)
{
	ODK_TRACE("ODK OnStop");
    return ODK_SUCCESS;
}



/*
	Funci�n Square que calcula el cuadrado de la variable recibida y la devuelve
	a traves de la variable de salida
*/
ODK_RESULT Square(/*IN*/const ODK_UINT32& intIn, /*OUT*/ODK_UINT32& intOut, /*OUT*/ODK_UINT32& estado, /*OUT*/ODK_UINT32& directo) {
	ODK_TRACE("-->Funcion");
	static bool creaVM = true;
	directo = intIn;
	if (creaVM) {
		JNIinit();
		creaVM = 0;
	}
	ODK_TRACE(" Ejecuta el metodo");
	jint square = env->CallStaticIntMethod(cls, mid, long(intIn));
	intOut = square;

	ODK_TRACE("<--Funcion");
	return ODK_SUCCESS;
}
//	ODK_INT32-->int32_t

/*	Funcion para calcular el cuadrado a trav�s de JNI
	Recibe los punteros del valor de entrada y de salida
*/
void JNIinit(void) {
	ODK_TRACE("Creando variables ODK");
	JavaVMOption options[1];
	JavaVMInitArgs vm_args;

	//options[0].optionString = "-Djava.class.path=.";
	options[0].optionString = "-Djava.class.path=D:\\Documentos personales\\Adrian\\Master\\TFM\\Pruebas\\JNI en ODK";
	memset(&vm_args, 0, sizeof(vm_args));
	vm_args.version = JNI_VERSION_1_6;
	vm_args.nOptions = 1;
	vm_args.options = options;
	long status = JNI_CreateJavaVM(&jvm, (void**)&env, &vm_args);
	if (status == JNI_ERR) {
		ODK_TRACE("Error creando la JavaVM");
		return;
	}

	cls = env->FindClass("Sample2");
	if (cls == NULL) {
		ODK_TRACE("Error buscando la clase");
		return;
	}
	//Metodo 2
	mid = env->GetStaticMethodID(cls, "intMethod", "(I)I");
	if (mid == 0) {
		ODK_TRACE("Error buscando el metodo");
		return;
	}
	ODK_TRACE("Variables ODK iniciadas correctamente");
}


ODK_RESULT readStruct(/*IN*/const adrStruct& structIn, /*OUT*/ODK_BOOL& boolOut, /*OUT*/ODK_INT32& intOut, /*OUT*/ODK_FLOAT& floatOut) {
	boolOut = structIn.boolVal;
	intOut = structIn.intVal + 1;
	floatOut = -structIn.floatVal;
	return ODK_SUCCESS;
}