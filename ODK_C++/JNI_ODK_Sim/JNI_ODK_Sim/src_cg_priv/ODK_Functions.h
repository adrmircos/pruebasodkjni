/*
 * This file is AUTO GENERATED - DO NOT MODIFY this file. 
 * This file contains the function prototypes of ODK 1500S.
 *
 * File created by ODK_CodeGenerator version 205.100.101.18
 * at Wed April 29 21:47:19 2020 
*/

#if !defined ODK_Functions_H
#define ODK_Functions_H

#include "ODK_Types.h"

#ifdef DLL_EXPORT
  #define EXPORT_API extern "C" __declspec(dllexport)
#else
  #define EXPORT_API extern "C"
#endif

// Basic function in order to show 
// how to create a function in ODK 1500S.
ODK_RESULT Square (
  /*IN*/const ODK_UINT32& intIn, // integer value as input
  /*OUT*/ODK_UINT32& intOut, // integer value as output
  /*OUT*/ODK_UINT32& estado, //Para saber hasta que punto funciona
  /*OUT*/ODK_UINT32& directo); //Copia directa de la entrada
#define _ODK_FUNCTION_SQUARE  ODK_RESULT Square (/*IN*/const ODK_UINT32& intIn, /*OUT*/ODK_UINT32& intOut, /*OUT*/ODK_UINT32& estado, /*OUT*/ODK_UINT32& directo)

ODK_RESULT readStruct (
  /*IN*/const adrStruct& structIn,
  /*OUT*/ODK_BOOL& boolOut,
  /*OUT*/ODK_INT32& intOut,
  /*OUT*/ODK_FLOAT& floatOut);
#define _ODK_FUNCTION_READSTRUCT  ODK_RESULT readStruct (/*IN*/const adrStruct& structIn, /*OUT*/ODK_BOOL& boolOut, /*OUT*/ODK_INT32& intOut, /*OUT*/ODK_FLOAT& floatOut)

ODK_RESULT GetTrace (
  /*IN*/const ODK_INT16& TraceCount,
  /*OUT*/ODK_S7STRING TraceBuffer[256][127]);
#define _ODK_FUNCTION_GETTRACE  ODK_RESULT GetTrace (/*IN*/const ODK_INT16& TraceCount, /*OUT*/ODK_S7STRING TraceBuffer[256][127])

#endif